package pl.ncontent.xmlparser.xml;

import org.apache.commons.lang3.math.NumberUtils;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import pl.ncontent.xmlparser.web.Response;

import java.time.LocalDateTime;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

public class PostsHandler extends DefaultHandler {

    private Response response;

    PostsHandler() {
        response = new Response();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("row")) {
            response.incrementCountOfTotalPosts();
            if (NumberUtils.isCreatable(attributes.getValue("Score"))) {
                response.updateTotalPostsScore(Long.valueOf(attributes.getValue("Score")));
            }
            if (attributes.getValue("AcceptedAnswerId") == null) {
                response.incrementCountOfTotalAcceptedPosts();
            }
            LocalDateTime creationDate = LocalDateTime.from(ISO_LOCAL_DATE_TIME.parse(attributes.getValue("CreationDate")));
            if (response.getFirstPost() == null || response.getFirstPost().isAfter(creationDate)) {
                response.setFirstPost(creationDate);
            }
            if (response.getLastPost() == null || response.getLastPost().isBefore(creationDate)) {
                response.setLastPost(creationDate);
            }
        }

    }

    @Override
    public void characters(char[] ch, int start, int length) {
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
    }

    public Response getResponse() {
        return response;
    }
}
