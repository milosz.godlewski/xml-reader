package pl.ncontent.xmlparser.xml;

import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import pl.ncontent.xmlparser.web.Response;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Service
public class XMLManager {
    public Response load(String url) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            Response response;
            SAXParser parser = factory.newSAXParser();
            InputStream input = new URL(url).openStream();
            PostsHandler pageHandler = new PostsHandler();
            parser.parse(input, pageHandler);
            response = pageHandler.getResponse();
            response.updateAvgScore();
            return response;
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException();
    }
}
