package pl.ncontent.xmlparser.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ncontent.xmlparser.xml.XMLManager;

import java.util.Map;
import java.util.Optional;

@RestController
class ReaderResource {

    private XMLManager xmlManager;

    public ReaderResource(XMLManager xmlManager) {
        this.xmlManager = xmlManager;
    }

    @PostMapping("/analyze/")
    public Optional<Response> processDocument(@RequestBody Map<String, String> data) {
        return Optional.ofNullable(xmlManager.load(data.get("url")));
    }

}
