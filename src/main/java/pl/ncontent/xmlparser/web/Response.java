package pl.ncontent.xmlparser.web;

import java.time.LocalDateTime;

public class Response {
    private LocalDateTime firstPost;
    private LocalDateTime lastPost;
    private Long totalPosts;
    private Integer totalAcceptedPosts;
    private Long avgScore;
    private Long totalScore;
    private LocalDateTime analyseDate;

    public Response() {
        totalPosts = 0L;
        totalAcceptedPosts = 0;
        avgScore = 0L;
        totalScore = 0L;
        analyseDate = LocalDateTime.now();
    }

    public LocalDateTime getFirstPost() {
        return firstPost;
    }

    public void setFirstPost(LocalDateTime firstPost) {
        this.firstPost = firstPost;
    }

    public LocalDateTime getLastPost() {
        return lastPost;
    }

    public void setLastPost(LocalDateTime lastPost) {
        this.lastPost = lastPost;
    }

    public void incrementCountOfTotalPosts() {
        this.totalPosts=this.totalPosts+1L;
    }

    public Long getTotalPosts() {
        return totalPosts;
    }

    public void incrementCountOfTotalAcceptedPosts() {
        this.totalAcceptedPosts++;
    }

    public void updateTotalPostsScore(Long totalScore) {
        this.totalScore += totalScore;
    }

    public void updateAvgScore() {
        avgScore = totalScore / totalPosts;
    }

    public Integer getTotalAcceptedPosts() {
        return totalAcceptedPosts;
    }

    public Long getAvgScore() {
        return avgScore;
    }

    public LocalDateTime getAnalyseDate() {
        return analyseDate;
    }

    @Override
    public String toString() {
        return "Response{" +
                "firstPost=" + firstPost +
                ", lastPost=" + lastPost +
                ", totalPosts=" + totalPosts +
                ", totalAcceptedPosts=" + totalAcceptedPosts +
                ", avgScore=" + avgScore +
                ", totalScore=" + totalScore +
                ", analyseDate=" + analyseDate +
                '}';
    }
}
