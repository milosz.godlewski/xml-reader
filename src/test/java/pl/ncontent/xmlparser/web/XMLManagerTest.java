package pl.ncontent.xmlparser.web;

import org.junit.jupiter.api.Test;
import pl.ncontent.xmlparser.xml.XMLManager;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class XMLManagerTest {
    private static final String MOCK_URL = "https://s3-eu-west-1.amazonaws.com/merapar-assessment/3dprinting-posts.xml";

    @Test
    void shouldLoadSuccessfully() {
        //given
        XMLManager xmlManager = new XMLManager();

        //when
        Response response = xmlManager.load(MOCK_URL);

        //then
        assertTrue(Objects.nonNull(response));
    }
}
