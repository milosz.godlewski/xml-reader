package pl.ncontent.xmlparser.web;

import org.junit.jupiter.api.Test;
import pl.ncontent.xmlparser.xml.XMLManager;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ReaderResourceTest {

    private static final String MOCK_URL = "https://s3-eu-west-1.amazonaws.com/merapar-assessment/3dprinting-posts.xml";

    @Test
    void shouldLoadSuccessfully() {
        //given
        XMLManager xmlManager = mock(XMLManager.class);
        ReaderResource readerResource = new ReaderResource(xmlManager);
        Map<String, String> map = new HashMap();
        map.put("url", MOCK_URL);

        //when
        Optional<Response> response = readerResource.processDocument(map);

        //then
        verify(xmlManager).load(MOCK_URL);
    }
}