build:
mvn package
run:
mvn spring-boot:run

docker build:
docker build -t xml-reader:0.0.1 .

docker run:
docker run -p 8080:8080 xml-reader:0.0.1